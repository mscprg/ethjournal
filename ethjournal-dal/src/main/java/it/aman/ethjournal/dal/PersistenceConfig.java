package it.aman.ethjournal.dal;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = {"it.aman.ethjournal.dal.repositories"})
@EntityScan(basePackages = {"it.aman.ethjournal.dal.entities"})
@EnableTransactionManagement
public class PersistenceConfig {
	
}
