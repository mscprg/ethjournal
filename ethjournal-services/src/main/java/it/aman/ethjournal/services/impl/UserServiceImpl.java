package it.aman.ethjournal.services.impl;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.aman.ethjournal.dal.entities.User;
import it.aman.ethjournal.dal.repositories.UserRepository;
import it.aman.ethjournal.services.IUserService;
import it.aman.ethjournal.services.dto.UserDto;


@Service
public class UserServiceImpl implements IUserService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	ModelMapper mapper;
	
	@Override
	public UserDto getUserById(Long userId) {
		Optional<User> foundUser = userRepo.findById(userId);
		UserDto user = new UserDto();
		user = mapper.map(foundUser.get(), UserDto.class);
		return user;
	}

}
