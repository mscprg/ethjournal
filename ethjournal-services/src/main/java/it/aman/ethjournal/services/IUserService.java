package it.aman.ethjournal.services;

import it.aman.ethjournal.services.dto.UserDto;

public interface IUserService {

	public UserDto getUserById(Long userId);
}
