package it.aman.ethjournal.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan(basePackages = {"it.aman.ethjournal"})
public class WebConfig implements WebMvcConfigurer  {
}
