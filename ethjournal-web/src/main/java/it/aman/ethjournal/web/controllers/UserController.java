package it.aman.ethjournal.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.aman.ethjournal.services.IUserService;
import it.aman.ethjournal.services.dto.UserDto;


@RestController
public class UserController {

	@Autowired
	IUserService userService;
	
	@RequestMapping(value = "/user/{id}",method = RequestMethod.GET)
	public String getUserDetail(@PathVariable("id") Long userId) {
		//a data from service layer
		UserDto user = userService.getUserById(userId);
		
		return user.toString();
	}
}
